const addNewPost = () => {
  cy.get('[data-cy=post-input]').type('test entry2');
    cy.get('[data-cy=post-button]').click();
}

describe('Post list', () => {
  beforeEach(() => {
    cy.exec('npm run dev:wipe-db');
    cy.visit('/');

  });
  
  it('new post appears in all posts list', () => {
    addNewPost();
    
    cy.get('[data-cy=post-body]').eq(0).should('contain', 'test entry2');
  });
  
  it('like number increased when click like', () => {
    addNewPost();
    cy.get('[data-cy=post-likes] div').eq(0).then(($likes) => {
      const num = parseInt($likes.text());
      cy.get('[data-cy=post-likes] button').eq(0).click(() => {
        const num2 = parseInt($likes.text());

        expect(num2).to.eq(num + 1);
      });
    });
  });
  
  it('post is removed from the list when click delete', () => {
    addNewPost();
    cy.get('[data-cy=post-delete]').click();

    cy.get('[data-cy=post-entry]').should(($posts) => {
      expect($posts).to.have.length(0);
    })
  })

  it('new comment appears under the post', () => {
    addNewPost();
    cy.get('[data-cy=comment-input]').type('I do agree');
    cy.get('[data-cy=comment-button]').click();

    cy.get('[data-cy=post-body]').eq(0).get('[data-cy=comment-body]').eq(0).should('contain', 'I do agree');
  })
});
