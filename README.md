## Available Scripts

### First time local env setup:

Install mongo db (3.6) example `brew install mongodb`
Install node modules in this directory and in server dir `npm i && (cd server && npm i)`

In the project directory, you can run:

### `npm run start:dev`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

### `npm rum cypress`

Starts dev server, client, MongoDB, opens Cypress app.

### `npm rum build`

Builds app for production.

### `npm run test`

Runs unit tests with jest

### `npm run e2e`

Runs e2e tests with cypress

## App info

Color pallete - https://coolors.co/eeeeff-7f7caf-bbacc1-80727b-7a82ab
