import React, { Component } from 'react';
import styles from './App.module.css';
import PostList from '../feed/PostList';
import NewPost from '../feed/NewPost';
import socketIOClient from 'socket.io-client';
import { withCurrentUser } from '../authentication/current-user-context';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      posts: [],
      postsLoaded: false,
      loggedInAs: 'Anonymous',
      loggedIn: false,
      comments: [],
      commentsLoaded: false
    };
  }

  componentDidMount() {
    const url =
      process.env.NODE_ENV === 'production' ? '/' : 'http://localhost:5001';
    const socket = socketIOClient.connect(url);
    this.fetchPosts();
    this.fetchComments();
    socket.on('likes', data => {
      this.setState({
        posts: this.state.posts.map(post => {
          if (post._id === data.postId) {
            post.likes += 1;
          }
          return post;
        })
      });
    });
    socket.on('add_post', data => {
      this.setState({ posts: [data.post, ...this.state.posts] });
    });
    socket.on('delete_post', data => {
      this.setState({
        posts: this.state.posts.filter(post => post._id !== data.postId)
      });
    });
    socket.on('add_comment', data => {
      this.setState({ comments: [...this.state.comments, data.comment] });
    });
  }

  fetchPosts() {
    fetch('/api/posts')
      .then(results => results.json())
      .then(data =>
        this.setState({ posts: data.posts }, () => {
          this.setState({ postsLoaded: true });
        })
      );
  }

  addPost = async post => {
    const newPost = {
      body: post,
      likes: 0,
      created: this.getDate(),
      author: this.props.user.name
    };
    await this.callAPI('/posts', 'POST', newPost);
  };

  likePost = async key => {
    await this.callAPI(`/posts/${key}`, 'PUT');
  };

  deletePost = async key => {
    await this.callAPI(`/posts/${key}`, 'DELETE');
    this.setState({
      posts: this.state.posts.filter(post => post._id !== key)
    });
  };

  addComment = async comment => {
    const newComment = {
      comment: comment.comment,
      created: this.getDate(),
      author: this.props.user.name
    };
    const postId = comment.postId;
    await this.callAPI(`/posts/${postId}/comments`, 'POST', newComment);
  };

  fetchComments = async () => {
    const result = await this.callAPI('/comments', 'GET');
    const data = await result.json();
    this.setState({ comments: data.comments }, () => {
      this.setState({ commentsLoaded: true });
    });
  };

  callAPI(route, method, data) {
    return fetch(`/api${route}`, {
      method: method,
      body: JSON.stringify(data),
      headers: { 'Content-Type': 'application/json' }
    });
  }

  getDate = () => {
    return new Date().toLocaleDateString('lt', {
      hour: '2-digit',
      minute: '2-digit',
      second: '2-digit'
    });
  };

  render() {
    return (
      <div>
        <div className={styles.postFeed}>
          <NewPost addPost={this.addPost} />
          <PostList
            posts={this.state.posts}
            postsLoaded={this.state.postsLoaded}
            likePost={this.likePost}
            deletePost={this.deletePost}
            addComment={this.addComment}
            comments={this.state.comments}
          />
        </div>
      </div>
    );
  }
}

export default withCurrentUser(App);
