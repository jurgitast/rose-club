import React from 'react';
import ReactDOM from 'react-dom';
import styles from './index.module.css';
import App from './app/App';
import Statistics from './statistics/Statistics';
import Privacy from './privacy/Privacy';
import * as serviceWorker from './serviceWorker';
import { BrowserRouter as Router, Link, Route } from 'react-router-dom';
import { CurrentUserProvider } from './authentication/current-user-context';
import Header from './feed/Header';

ReactDOM.render(
  <Router>
    <CurrentUserProvider>
      <div className={styles.app}>
        <Header />
        <div className={styles.topMenu}>
          <ul>
            <li>
              <Link to='/'>Posts</Link>
            </li>
            <li>
              <Link to='/stat'>Statistics</Link>
            </li>
          </ul>
        </div>
        <div className={styles.content}>
          <Route exact={true} path='/' component={App} />
          <Route path='/stat' component={Statistics} />
          <Route path='/privacy' component={Privacy} />
        </div>
      </div>
    </CurrentUserProvider>
  </Router>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
