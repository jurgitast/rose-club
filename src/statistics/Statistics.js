import React, { Component } from 'react';
import styles from './Statistics.module.css';
import PieChart from './PieChart';

class Statistics extends Component {
  constructor(props) {
    super(props);
    this.state = {
      totalPosts: 0,
      totalComments: 0,
      topPosts: [],
      isLoggedIn: false
    };
  }

  componentDidMount() {
    this.getTotalPosts();
    this.getTotalComments();
    this.getTotalLikes();
    this.getTopPosts();
  }

  getTotalPosts = async () => {
    const total = await this.callAPI('/stats/posts-count', 'GET');
    const data = await total.json();
    this.setState({ totalPosts: data.count });
  };

  getTotalComments = async () => {
    const total = await this.callAPI('/stats/comments-count', 'GET');
    const data = await total.json();
    this.setState({ totalComments: data.count });
  };

  getTotalLikes = async () => {
    const total = await this.callAPI('/stats/likes-count', 'GET');
    const data = await total.json();
    this.setState({ totalLikes: data.count });
  };

  getTopPosts = async () => {
    const top = await this.callAPI('/stats/top-posts', 'GET');
    const data = await top.json();
    this.setState({ topPosts: data.posts });
  };

  callAPI(route, method, data) {
    return fetch(`/api${route}`, {
      method: method,
      body: JSON.stringify(data),
      headers: { 'Content-Type': 'application/json' }
    });
  }

  addUser = user => {
    this.setState({ isLoggedIn: true });
    console.log(this.state.isLoggedIn);
  };

  render() {
    const { totalPosts, totalComments, topPosts, totalLikes } = this.state;
    return (
      <div>
        <div className={styles.statistics}>
          <div className={styles.totalPosts}>Total posts: {totalPosts}</div>
          <div className={styles.totalComments}>
            Total comments: {totalComments}
          </div>
          <div className={styles.totalLikes}>Total likes: {totalLikes}</div>
          <div className={styles.pie}>
            <PieChart
              totalPosts={totalPosts}
              totalComments={totalComments}
              totalLikes={totalLikes}
            />
          </div>
          <div className={styles.topPosts}>
            Most commented posts:
            {topPosts.map(p => {
              return (
                <div key={p._id} className={styles.post}>
                  <div className={styles.postBody}>{p.body}</div>
                  <div className={styles.postAuthor}> by {p.author}</div>
                  <div className={styles.postCreated}>{p.created}</div>
                  <div className={styles.postComments}>
                    # comments {p.comments}
                  </div>
                </div>
              );
            })}
          </div>
        </div>
      </div>
    );
  }
}

export default Statistics;
