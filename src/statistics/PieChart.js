import React from 'react';
import { Pie } from 'react-chartjs-2';

const PieChart = ({ totalPosts, totalComments, totalLikes }) => {
  const fontFamily = `'Open Sans', sans-serif`;
  const data = {
    labels: ['Posts', 'Comments', 'Likes'],
    datasets: [
      {
        backgroundColor: ['#7A82AB', '#80727B', '#BBACC1'],
        data: [totalPosts, totalComments, totalLikes]
      }
    ]
  };
  const options = {
    title: {
      display: false,
      text: 'Predicted world population (millions) in 2050'
    },
    tooltips: {
      bodyFontColor: '#EEEEFF',
      bodyFontFamily: fontFamily,
      bodyFontSize: 12
    },
    legend: {
      labels: {
        fontColor: '#000',
        fontFamily,
        fontSize: 14
      }
    }
  };

  return (
    <Pie
      data={data}
      options={options}
      width={500}
      height={500}
      maintainAspectRatio={true}
    />
  );
};

export default PieChart;
