import React, { createContext } from 'react';

const UserContext = createContext({
  updateUser: () => {},
  user: null
});

export class CurrentUserProvider extends React.Component {
  updateUser = user => {
    this.setState({ user });
  };

  state = {
    user: {
      isLoggedIn: false,
      userId: '',
      name: 'Anonymous',
      email: '',
      picture: ''
    },
    updateUser: this.updateUser
  };

  render() {
    return (
      <UserContext.Provider value={this.state}>
        {this.props.children}
      </UserContext.Provider>
    );
  }
}

export const withCurrentUser = WrappedComponent => {
  return props => (
    <CurrentUserConsumer>
      {({ user, updateUser }) => (
        <WrappedComponent user={user} updateUser={updateUser} {...props} />
      )}
    </CurrentUserConsumer>
  );
};

export const CurrentUserConsumer = UserContext.Consumer;
