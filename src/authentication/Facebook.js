import React, { Component } from 'react';
import FacebookLogin from 'react-facebook-login';
import styles from './facebook.module.css';
import { withCurrentUser } from './current-user-context';

class FaceBook extends Component {
  responseFacebook = response => {
    const user = {
      isLoggedIn: true,
      userId: response.userId,
      name: response.name,
      email: response.email,
      picture: response.picture.data.url
    };
    this.props.updateUser(user);
  };

  renderContent(content) {
    return (
      <div
        className={styles.fbLogin}
        //style={{ display: loading ? 'none' : 'block' }}
      >
        {content}
      </div>
    );
  }

  render() {
    let { user } = this.props;

    if (user.isLoggedIn) {
      return this.renderContent(
        <div className={styles.fbInfo}>
          <img
            className={styles.fbPicture}
            src={user.picture}
            alt={user.name}
            title={user.name}
          />
          <div className={styles.fbName}> Logged in as {user.name} </div>
        </div>
      );
    }

    return this.renderContent(
      <FacebookLogin
        appId='793894964313424'
        autoLoad={true}
        fields='name,email,picture'
        callback={this.responseFacebook}
        size='small'
        isMobile={false}
      />
    );
  }
}

export default withCurrentUser(FaceBook);
