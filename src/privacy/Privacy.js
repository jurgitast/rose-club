import React from 'react';

const Privacy = () => {
  return (
    <div>
      When you sign up to the rose-club app, you give us permission to use your
      name, profile picture and email. We use this information to identify the
      content you have written.
    </div>
  );
};

export default Privacy;
