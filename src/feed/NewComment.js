import React, { Component } from 'react';
import styles from './NewComment.module.css';
class NewComment extends Component {
  constructor(props) {
    super(props);
    this.state = {
      comment: ''
    };
  }

  handleChange = event => {
    this.setState({ comment: event.target.value });
  };

  handleClick = () => {
    if (this.state.comment !== '') {
      this.props.postComment({
        comment: this.state.comment,
        postId: this.props.postId
      });
      this.setState({ comment: '' });
    }
  };

  render() {
    return (
      <div className={styles.comment}>
        <textarea
          data-cy='comment-input'
          placeholder='Your comment'
          className={styles.commentInput}
          onChange={this.handleChange}
          value={this.state.comment}
          rows='4'
          cols='20'
        />
        <button
          data-cy='comment-button'
          className={styles.commentButton}
          onClick={this.handleClick}
        >
          Comment
        </button>
      </div>
    );
  }
}

export default NewComment;
