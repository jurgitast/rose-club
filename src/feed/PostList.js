import React, { Component } from 'react';
import NewComment from './NewComment';
import styles from './PostList.module.css';
import Icon from 'react-icons-kit';
import { thumbsOUp } from 'react-icons-kit/fa/thumbsOUp';
import { trashO } from 'react-icons-kit/fa/trashO';
import FlipMove from 'react-flip-move';

class Posts extends Component {
  handleClick = comment => {
    this.props.addComment(comment);
  };

  renderPost = post => {
    return (
      <div data-cy='post-entry' className={styles.postEntry} key={post._id}>
        <div className={styles.postDetails}>
          <div className={styles.postAuthor}>{post.author}</div>
          <div className={styles.postDate}>{post.created}</div>
        </div>
        <div data-cy='post-body' className={styles.postBody}>
          {post.body}
        </div>
        <div className={styles.postActions}>
          <div data-cy='post-likes' className={styles.postLikes}>
            <div className={styles.likesCount}>{post.likes}</div>
            <button onClick={() => this.props.likePost(post._id)}>
              <Icon icon={thumbsOUp} />
            </button>
          </div>
          <div className={styles.postDelete}>
            <button
              data-cy='post-delete'
              onClick={() =>
                window.confirm('Are you sure you want to delete this entry?') &&
                this.props.deletePost(post._id)
              }
            >
              <Icon icon={trashO} />
            </button>
          </div>
        </div>
        <div className={styles.comments}>
          <FlipMove disableAllAnimations={!this.props.commentsLoaded}>
            {this.props.comments
              .filter(com => com.postId === post._id)
              .map(result => {
                return (
                  <div
                    data-cy='comment-body'
                    className={styles.commentBody}
                    key={result._id}
                  >
                    <div className={styles.commentDetails}>
                      <div className={styles.commentAuthor}>
                        {result.author}
                      </div>
                      <div className={styles.commentDate}>{result.created}</div>
                    </div>
                    <div className={styles.commentText}>{result.comment}</div>
                  </div>
                );
              })}
          </FlipMove>
        </div>
        <NewComment postId={post._id} postComment={this.handleClick} />
      </div>
    );
  };

  render() {
    if (!this.props.posts) {
      return null;
    }

    return (
      <div>
        <FlipMove disableAllAnimations={!this.props.postsLoaded}>
          {this.props.posts.map(this.renderPost)}
        </FlipMove>
      </div>
    );
  }
}
export default Posts;
