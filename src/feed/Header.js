import React, { Component } from 'react';
import Facebook from '../authentication/Facebook';
import styles from './Header.module.css';
import rose from '../rose.png';

class Header extends Component {
  render() {
    return (
      <div>
        <header className={styles.feedHeader}>
          <div className={styles.fbLogin}>
            <Facebook />
          </div>
          <div className={styles.title}>
            <div className={styles.titleText}>Rose Feed</div>
            <img className={styles.rose} src={rose} alt='Rose' />
          </div>
        </header>
      </div>
    );
  }
}

export default Header;
