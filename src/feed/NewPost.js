import React, { Component } from 'react';
import styles from './NewPost.module.css';

class NewPost extends Component {
  constructor(props) {
    super(props);
    this.state = {
      post: ''
    };

    this.handleClick = this.handleClick.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(event) {
    this.setState({ post: event.target.value });
  }

  handleClick(event) {
    event.preventDefault();
    if (this.state.post !== '') {
      this.props.addPost(this.state.post);
      this.setState({ post: '' });
    }
  }

  render() {
    return (
      <div className={styles.post}>
        <textarea
          data-cy='post-input'
          onChange={this.handleChange}
          value={this.state.post}
          className={styles.postInput}
          placeholder="What's on your rosy mind?"
          rows='6'
          cols='30'
        />
        <br />
        <button
          data-cy='post-button'
          className={styles.postButton}
          onClick={this.handleClick}
        >
          Post
        </button>
      </div>
    );
  }
}
export default NewPost;
