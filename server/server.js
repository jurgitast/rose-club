const path = require('path');
const fs = require('fs');
const express = require('express');
const socket = require('socket.io');
const app = express();
const bodyParser = require('body-parser');
const { create } = require('./storage');

const port = process.env.PORT || 5001;

const start = async () => {
  const storage = await create();

  const server = app.listen(port, () =>
    console.log(`Listening on port ${port}`)
  );

  app.use(bodyParser.urlencoded({ extended: false }));
  app.use(bodyParser.json());

  const io = socket(server);

  app.get('/api/posts', async (req, res) => {
    try {
      const posts = await storage.findPosts();
      return res.send({ posts: posts.reverse() });
    } catch (e) {
      return res.status(500).send(`DB failed: ${e}`);
    }
  });

  app.get('/api/stats/posts-count', async (req, res) => {
    const postsCount = await storage.countPosts();

    return res.send({ count: postsCount });
  });

  app.get('/api/stats/comments-count', async (req, res) => {
    const commentsCount = await storage.countComments();

    return res.send({ count: commentsCount });
  });

  app.get('/api/stats/likes-count', async (req, res) => {
    const likesCount = await storage.countLikes();

    return res.send({ count: likesCount });
  });

  app.get('/api/stats/top-posts', async (req, res) => {
    const top = await storage.topPosts();
    const ids = top.map(p => p._id);
    const posts = await storage.findPostsIds(ids);
    const orderedPosts = top.map(t => {
      const post = posts.find(p => p._id === t._id);
      post.comments = t.count;
      return post;
    });

    return res.send({ posts: orderedPosts });
  });

  app.post('/api/posts', async (req, res) => {
    const post = { ...req.body, _id: storage.genId() };
    await storage.insertPost(post);
    io.sockets.emit('add_post', { post });

    res.status(200).send(post._id);
  });

  app.put('/api/posts/:postId', async (req, res) => {
    const postId = req.params.postId;
    const result = await storage.incrementPostLikes(postId);

    if (result.modifiedCount > 0) {
      const post = await storage.findPost(postId);
      io.sockets.emit('likes', { postId, likes: post[0].likes });
      return res.sendStatus(200);
    }
    return res.status(404).send('No items found to update');
  });

  app.delete('/api/posts/:postId', async (req, res) => {
    const postId = req.params.postId;
    const comments = await storage.findComments(postId);
    comments.forEach(comment => storage.deleteComment(comment._id));

    const result = await storage.deletePost(postId);
    if (result.deletedCount > 0) {
      io.sockets.emit('delete_post', { postId });
      return res.sendStatus(200);
    }
    return res.status(404).send('No items found to delete');
  });

  app.get('/api/posts/:postId/comments', async (req, res) => {
    try {
      const postId = req.params.postId;
      const comments = await storage.findComments(postId);
      return res.send({ comments });
    } catch (e) {
      return res.status(500).send(`DB failed: ${e}`);
    }
  });

  app.get('/api/comments', async (req, res) => {
    try {
      const comments = await storage.findAllComments();
      return res.send({ comments });
    } catch (e) {
      return res.status(500).send(`DB failed: ${e}`);
    }
  });

  app.post('/api/posts/:postId/comments', async (req, res) => {
    const postId = req.params.postId;
    const comment = { ...req.body, _id: storage.genId(), postId };
    const post = await storage.findPost(postId);

    if (post.length === 1) {
      await storage.insertComment(comment);
      io.sockets.emit('add_comment', { comment });
      res.sendStatus(201);
    } else {
      res.sendStatus(404);
    }
  });

  const buildDir = path.join(__dirname, '../build');

  if (fs.existsSync(buildDir)) {
    // Serve any static files
    app.use(express.static(buildDir));

    // Handle React routing, return all requests to React app
    app.get('*', function(req, res) {
      res.sendFile(path.join(buildDir, 'index.html'));
    });
  }

  return {
    stop: () => server.close()
  };
};

module.exports = {
  start
};
