const MongoClient = require('mongodb').MongoClient;
const ObjectId = require('mongodb').ObjectId;

const dbName = process.env.DB_NAME || 'rose';
const dbPort = process.env.DB_PORT || 27017;
const dbURL = process.env.DB_URL || `mongodb://localhost:${dbPort}`;

const genId = () => new ObjectId().toHexString();

const create = async () => {
  const client = await MongoClient.connect(dbURL, { useNewUrlParser: true });
  const db = client.db(dbName);

  return {
    findPosts: () =>
      db
        .collection('posts')
        .find()
        .toArray(),
    findPost: id =>
      db
        .collection('posts')
        .find({ _id: id })
        .toArray(),
    countPosts: () => db.collection('posts').countDocuments(),
    topPosts: () =>
      db
        .collection('comments')
        .aggregate([
          {
            $group: {
              _id: '$postId',
              count: { $sum: 1 }
            }
          },
          {
            $sort: {
              count: -1
            }
          },
          {
            $limit: 3
          }
        ])
        .toArray(),
    findPostsIds: ids =>
      db
        .collection('posts')
        .find({ _id: { $in: ids } })
        .toArray(),
    insertPost: post => db.collection('posts').insertOne(post),
    incrementPostLikes: id =>
      db.collection('posts').updateOne({ _id: id }, { $inc: { likes: 1 } }),
    deletePost: id => db.collection('posts').deleteOne({ _id: id }),
    insertComment: comment => db.collection('comments').insertOne(comment),
    findAllComments: () =>
      db
        .collection('comments')
        .find()
        .toArray(),
    countComments: () => db.collection('comments').countDocuments(),
    countLikes: async () => {
      const totals = await db
        .collection('posts')
        .aggregate([
          {
            $group: {
              _id: '$postId',
              count: { $sum: '$likes' }
            }
          }
        ])
        .toArray();
      return totals.length > 0 ? totals[0].count : 0;
    },
    findComments: id =>
      db
        .collection('comments')
        .find({ postId: id })
        .toArray(),
    deleteComment: id => db.collection('comments').deleteOne({ _id: id }),
    genId
  };
};

module.exports = {
  create
};
