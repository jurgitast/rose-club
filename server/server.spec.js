const {
  givenPosts,
  loadPosts,
  givenComments,
  loadComments,
  randomId
} = require('./test/db-driver');
const server = require('./server');
const axios = require('axios').create({
  validateStatus: status => true
});
const { omit } = require('lodash');

describe('server', () => {
  let app;
  beforeAll(async () => {
    app = await server.start();
  });
  afterAll(() => app.stop());

  describe('GET /posts', () => {
    it('get empty list', async () => {
      const { status, data } = await axios.get(
        'http://localhost:5001/api/posts'
      );

      expect(status).toEqual(200);
      expect(data.posts).toEqual([]);
    });

    it('get posts', async () => {
      const post1Id = randomId();
      const post2Id = randomId();
      const post1 = { _id: post1Id, title: 'Labukas' };
      const post2 = { _id: post2Id, title: 'Kutis' };
      await givenPosts([post1, post2]);

      const { status, data } = await axios.get(
        'http://localhost:5001/api/posts'
      );

      expect(status).toEqual(200);
      expect(data.posts).toEqual([post2, post1]);
    });
  });

  describe('POST /posts', () => {
    it('create new post', async () => {
      const post = { title: 'Labukas' };

      const { status } = await axios.post(
        'http://localhost:5001/api/posts',
        post
      );

      expect(status).toEqual(200);

      const posts = await loadPosts();
      expect(posts).toHaveLength(1);
    });
  });

  describe('PUT /posts/:id', () => {
    it('set post like count to 1', async () => {
      const post = { _id: '123', title: 'Labukas' };
      await givenPosts([post]);

      const { status } = await axios.put('http://localhost:5001/api/posts/123');
      expect(status).toEqual(200);

      const posts = await loadPosts();
      expect(posts).toEqual([{ ...post, likes: 1 }]);
    });

    it('returns 404 error when post not found', async () => {
      const post = { _id: '123', title: 'Labukas' };
      await givenPosts([post]);

      const { status } = await axios.put('http://localhost:5001/api/posts/124');
      expect(status).toEqual(404);
    });
  });

  describe('DELETE /posts/:id', () => {
    it('delete the post', async () => {
      const post = { _id: randomId(), title: 'Labukas' };
      const comment1 = {
        _id: randomId(),
        comment: 'Labukas',
        postId: post._id
      };
      const comment2 = {
        _id: randomId(),
        comment: 'Labukas',
        postId: randomId()
      };
      await givenPosts([post]);
      await givenComments([comment1, comment2]);

      const { status } = await axios.delete(
        `http://localhost:5001/api/posts/${post._id}`
      );
      expect(status).toEqual(200);

      const posts = await loadPosts();
      expect(posts).toHaveLength(0);

      const comments = await loadComments();
      expect(comments).toEqual([comment2]);
    });

    it('returns 404 error when post not found', async () => {
      const post = { _id: '123', title: 'Labukas' };
      await givenPosts([post]);

      const { status } = await axios.delete(
        'http://localhost:5001/api/posts/124'
      );
      expect(status).toEqual(404);
    });
  });

  describe('POST /posts/:id/comments', () => {
    it('creates comment under the post with id', async () => {
      const post = { title: 'Labukas' };
      const insertedPost = await axios.post(
        'http://localhost:5001/api/posts',
        post
      );

      const commentInDb = { title: 'Labukas', postId: insertedPost.data };
      const comment = { title: 'Labukas' };

      const { status } = await axios.post(
        `http://localhost:5001/api/posts/${insertedPost.data}/comments`,
        comment
      );

      expect(status).toEqual(201);

      const comments = await loadComments();
      expect(comments.map(c => omit(c, '_id'))).toEqual([commentInDb]);
    });

    it('returns 404 if comment is posted under non existing post', async () => {
      const comment = { title: 'Labukas' };
      const { status } = await axios.post(
        `http://localhost:5001/api/posts/nonexistingid/comments`,
        comment
      );

      expect(status).toEqual(404);
    });
  });

  describe('Statistics', () => {
    it('counts amount of posts', async () => {
      const post1 = { title: 'labukas' };
      const post2 = { title: 'aciukas' };
      await givenPosts([post1, post2]);

      const { data } = await axios.get(
        'http://localhost:5001/api/stats/posts-count'
      );

      expect(data.count).toEqual(2);
    });

    it('counts amount of comments', async () => {
      const post = { _id: randomId(), title: 'Labukas' };
      await givenPosts([post]);

      const comment1 = { title: 'Labukas', postId: post._id };
      const comment2 = { title: 'Labukas2', postId: post._id };
      await givenComments([comment1, comment2]);

      const { data } = await axios.get(
        'http://localhost:5001/api/stats/comments-count'
      );

      expect(data.count).toEqual(2);
    });

    it('counts zero likes', async () => {
      const { data } = await axios.get(
        'http://localhost:5001/api/stats/likes-count'
      );

      expect(data.count).toEqual(0);
    });

    it('counts amount of likes', async () => {
      const post1 = { _id: randomId(), likes: 1 };
      const post2 = { _id: randomId(), likes: 3 };
      const post3 = { _id: randomId() };
      await givenPosts([post1, post2, post3]);
      const { data } = await axios.get(
        'http://localhost:5001/api/stats/likes-count'
      );

      expect(data.count).toEqual(4);
    });

    it('gets 3 most popular posts based on comments', async () => {
      const post1 = { _id: randomId(), title: 'labukas' };
      const post2 = { _id: randomId(), title: 'aciukas' };
      const post3 = { _id: randomId(), title: 'Andriukas' };
      const post4 = { _id: randomId(), title: 'Mildukas' };
      await givenPosts([post1, post2, post3, post4]);

      const comment1 = { title: 'Labukas', postId: post1._id };
      const comment2 = { title: 'Labukas2', postId: post2._id };
      const comment3 = { title: 'Labukas3', postId: post4._id };
      const comment4 = { title: 'Labukas4', postId: post4._id };
      await givenComments([comment1, comment2, comment3, comment4]);

      const { data } = await axios.get(
        'http://localhost:5001/api/stats/top-posts'
      );
      post4.comments = 2;
      post1.comments = 1;
      post2.comments = 1;
      expect(data.posts).toHaveLength(3);
      expect(data.posts).toEqual([post4, post1, post2]);
    });
  });
});
