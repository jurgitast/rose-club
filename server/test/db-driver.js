const { MongoClient, ObjectId } = require('mongodb');

const DB_PORT = process.env.DB_PORT;
const DB_NAME = process.env.DB_NAME;
const url = `mongodb://localhost:${DB_PORT}/${DB_NAME}`;

const randomId = () => new ObjectId().toHexString();

let client;
const connect = async () => {
  if (client) {
    return client;
  }
  client = await MongoClient.connect(url, { useNewUrlParser: true })
  return client;
}

const getCollection = async name => {
  const client = await connect();
  const coll = await client.db().collection(name);
  return {
    given: objects => coll.insertMany(objects),
    load: () => coll.find().toArray(),
  }
}

const given = collectionName => async entities => {
  const collection = await getCollection(collectionName);
  await collection.given(entities)
};
const load = collectionName => async () => {
  const collection = await getCollection(collectionName);
  return await collection.load();
}

module.exports = {
  givenPosts: given('posts'),
  loadPosts: load('posts'),
  givenComments: given('comments'),
  loadComments: load('comments'),
  randomId,
};