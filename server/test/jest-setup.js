const mongoUnit = require('mongo-unit');

const DB_PORT = process.env.DB_PORT;
const DB_NAME = process.env.DB_NAME;

const getDbUrl = () => `mongodb://localhost:${DB_PORT}/${DB_NAME}`;

const mongo = {
  start: () => mongoUnit.start({ port: DB_PORT, dbName: DB_NAME }),
  stop: () => mongoUnit.stop(),
  reset: () => mongoUnit.dropDb(getDbUrl())
}

global.beforeAll(() => mongo.start());
global.afterAll(() => mongo.stop());
global.beforeEach(() => mongo.reset());