const driver = require('./db-driver');

describe('db driver', () => {
  it('should insert and load posts', async () => {
    await driver.givenPosts([{ _id: 123, a: 1 }]);
    const posts = await driver.loadPosts();
    
    expect(posts).toEqual([{ _id: 123, a: 1 }]);
  });
})